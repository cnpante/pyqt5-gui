import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from main import Ui_MainWindow


class Tab1Process(Ui_MainWindow):   # Class that can call objects relative to Tab 1
    def __init__(self):
        self.tab1_browse_button.clicked.connect(self.do_something)

    def do_something(self):
        print("Did something.")

class Tab2Process(Ui_MainWindow):   # Class that can call objects relative to Tab 2
    def __init__(self):
        self.tab2_browse_button.clicked.connect(self.do_something_else)

    def do_something_else(self):
        print("Did something else.")


class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None):
        _translate = QCoreApplication.translate
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        self.tab1_process = Tab1Process() # This does not work, but I think I need this 'concept' to register the above classes
        self.tab1_process.exec_()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    app.exec_()